from app import db
from datetime import datetime

class Reading(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    pot_name = db.Column(db.String(64))
    date = db.Column(db.DateTime, nullable=False,default=datetime.utcnow)
    raw = db.Column(db.String(128))
    vol = db.Column(db.Integer)

    def __repr__(self):
        return '<Reading {}>'.format(self.id)   