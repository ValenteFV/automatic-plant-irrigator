# Automatic Water Irrigator 
Source code and notes on automatic plant irrigator. Using a raspberry pi, python and off the shelf circuits.

### Hardware
- Raspberry Pi, since this is a really low tech project any raspberry pi will do. I'm currently using Raspberry Pi 2 B v1.1 so anything above shoudl work 
- Wifi adapter 


### Steps

- Install Raspian I'm using [Buster Lite] and used [Raspberry Pi Imager] to install the image
- Setup the wireless functionality and SSH: plug the wifi adapter, turn the raspberry on, once in command line type: sudo raspi-config , once in the interactive menu you should be able to enable both easily. For more information visit the [wireless documentation] 
--while in raspi-config enable the I2C for the ADC to work, more [information on I2C]
- Install the following stuff (there will be a bashfile):
-- sudo apt-get install vim
-- sudo apt-get install python-pip
-- sudo apt-get install -y python-smbus
-- sudo apt-get install -y i2c-tools
-- pip install ADS1115
-- pip install adafruit-ads1x15


### Models

Model #1 - reading 
- ID
- pot_name
- date
- raw
- vol

### Notes
- [Post that inspired me]
- [Raspberry Pi pin information] 


[information on I2C]: <https://learn.adafruit.com/adafruits-raspberry-pi-lesson-4-gpio-setup/configuring-i2c>
[Post that inspired me]: <https://belikoff.net/using-raspberry-pi-to-water-plants/>
[Raspberry Pi pin information ]: <https://www.raspberrypi.org/documentation/usage/gpio/>
[wireless documentation]: <https://www.raspberrypi.org/documentation/configuration/wireless/wireless-cli.md>
[Raspberry Pi Imager]: <https://www.raspberrypi.org/downloads/>
[Buster Lite]: <https://www.raspberrypi.org/downloads/raspbian/>
